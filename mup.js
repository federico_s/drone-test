module.exports = {
    servers: {
        one: {
            host: '146.185.178.241',
            username: 'root',
            password: 'qwertyu12345',
        }
    },

    meteor: {
        name: 'meteor-mocha',
        path: '.',
        servers: {
            one: {}
        },
        buildOptions: {
            debug: true,
        },
        env: {
            ROOT_URL: 'http://146.185.135.7',
        },
        deployCheckWaitTime: 120 //default 10
    },
    docker: {
        image:'kadirahq/meteord',//optional
    },
    mongo: {
        oplog: true,
        port: 27017,
        servers: {
            one: {},
        },
    }
}
